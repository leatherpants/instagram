<?php

namespace App\Instagram\Images\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Instagram\Images\Models\Images.
 *
 * @property mixed|null $id
 * @property mixed|null $userid
 * @property string $type
 * @property string $url
 * @property int $width
 * @property int $height
 * @mixin \Eloquent
 */
class Images extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'userid',
        'type',
        'url',
        'width',
        'height',
    ];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'id' => 'biginteger',
        'userid' => 'biginteger',
    ];

    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = false;
}
