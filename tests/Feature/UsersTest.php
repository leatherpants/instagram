<?php

namespace Tests\Feature;

use Tests\TestCase;

class UsersTest extends TestCase
{
    /** @test */
    public function test_shows_user_profile()
    {
        $response = $this->get(route('user', ['username' => $this->user->username]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_user_profile_fails()
    {
        $response = $this->get(route('user', ['username' => 'xyz']));
        $response->assertNotFound();
    }

    /** @test */
    public function test_shows_that_the_user_is_verified()
    {
        $data = [
            'username' => $this->user->username,
            'is_verified' => true,
        ];

        $response = $this->get(route('user', ['username' => $data['username']]), $data);
        $response->assertSuccessful();

        $this->assertTrue($data['is_verified']);
    }

    /** @test */
    public function test_shows_that_the_user_has_a_bio()
    {
        $data = [
            'username' => $this->user->username,
            'bio' => $this->user->bio,
        ];

        $response = $this->get(route('user', ['username' => $data['username']]), $data);
        $response->assertSuccessful();

        $response->assertSeeText($data['bio']);
    }
}
