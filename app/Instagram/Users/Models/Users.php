<?php

namespace App\Instagram\Users\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Instagram\Users\Models\Users.
 *
 * @property int|null $id
 * @property string $username
 * @property string $full_name
 * @property string $profile_picture
 * @property string $bio
 * @property string|null $website
 * @property bool $is_active
 * @property bool $is_private
 * @property bool $is_verified
 * @mixin \Eloquent
 */
class Users extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'username',
        'full_name',
        'profile_picture',
        'bio',
        'website',
        'is_active',
        'is_private',
        'is_verified',
    ];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
        'is_private' => 'boolean',
        'is_verified' => 'boolean',
    ];

    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = false;
}
