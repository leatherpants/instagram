@extends('layouts.blank')
@section('content')
DB::table('images')->insert([
@foreach($images as $image)
    [
        'id' => {{ $image->id }},
        'userid' => {{ $image->userid }},
        'type' => '{{ $image->type }}',
        'url' => '{{ $image->url }}',
        'width' => {{ $image->width }},
        'height' => {{ $image->height }},
    ],
@endforeach
]);
@endsection