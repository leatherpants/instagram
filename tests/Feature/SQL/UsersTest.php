<?php

namespace Tests\Feature\SQL;

use Tests\TestCase;

class UsersTest extends TestCase
{
    /** @test */
    public function test_get_all_users()
    {
        $response = $this->get(route('sql.users'));
        $response->assertSuccessful();
    }
}
