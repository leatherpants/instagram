<?php

namespace App\Http\Controllers\SQL;

use App\Http\Controllers\Controller;
use App\Instagram\Images\Repositories\ImagesRepository;

class ImagesController extends Controller
{
    /**
     * @var ImagesRepository
     */
    private $imagesRepository;

    /**
     * ImagesController constructor.
     * @param ImagesRepository $imagesRepository
     */
    public function __construct(ImagesRepository $imagesRepository)
    {
        $this->imagesRepository = $imagesRepository;
    }

    /**
     * Get all images for seeders.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $images = $this->imagesRepository->getAllImages();

        return view('sql.images')
            ->with('images', $images);
    }
}
