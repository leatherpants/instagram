<?php

use Faker\Generator as Faker;

$factory->define(\App\Instagram\Images\Models\Images::class, function (Faker $faker) {
    return [
        'id' => $faker->randomNumber,
        'userid' => function() {
            return factory(\App\Instagram\Users\Models\Users::class)->create()->id;
        },
        'type' => 'standard_resolution',
        'url' => $faker->imageUrl(),
        'width' => 600,
        'height' => 600,
    ];
});
