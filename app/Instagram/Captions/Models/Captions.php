<?php

namespace App\Instagram\Captions\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Instagram\Captions\Models\Captions.
 *
 * @property mixed|null $id
 * @property int $created_time
 * @property string $text
 * @property mixed|null $from
 * @mixin \Eloquent
 */
class Captions extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'created_time',
        'text',
        'from',
    ];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'id' => 'biginteger',
        'from' => 'biginteger',
    ];

    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = false;
}
