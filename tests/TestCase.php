<?php

namespace Tests;

use App\Instagram\Users\Models\Users;
use App\Instagram\Images\Models\Images;
use App\Instagram\Captions\Models\Captions;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    protected $caption;
    protected $image;
    protected $user;

    /**
     * Set up the test.
     */
    public function setUp()
    {
        parent::setUp();

        $this->caption = factory(Captions::class)->create();
        $this->image = factory(Images::class)->create();
        $this->user = factory(Users::class)->create();
    }

    public function tearDown()
    {
        $this->artisan('migrate:reset');
        parent::tearDown();
    }
}
