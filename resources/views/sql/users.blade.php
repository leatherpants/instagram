@extends('layouts.blank')
@section('content')
DB::table('users')->insert([
@foreach($users as $user)
    [
        'id' => {{ $user->id }},
        'username' => '{{ $user->username }}',
        'full_name' => '{{ $user->full_name }}',
        'profile_picture' => '{{ $user->profile_picture }}',
        'bio' => '{{ $user->bio }}',
        'website' => '{{ $user->website }}',
        'is_active' => {{ ($user->is_active == 1) ? 'true' : 'false' }},
        'is_private' => {{ ($user->is_private == 1) ? 'true' : 'false' }},
        'is_verified' => {{ ($user->is_verified == 1) ? 'true' : 'false' }},
    ],
@endforeach
]);
@endsection