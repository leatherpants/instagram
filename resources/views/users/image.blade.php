@extends('layouts.frontend')
@section('title', $image->full_name.' (@'.$image->username.') • Instagram')
@section('content')
    <div class="ui container">
        <div class="ui stackable grid">
            <div class="two column row">
                <div class="ten wide column">
                    <img src="http://media.instagram.granath/www/big_resolution/{{ basename($image->url) }}" alt="">
                </div>
                <div class="six wide column">
                    <header>
                        <img src="{{ $image->profile_picture }}" alt="" class="ui avatar image">
                        <a href="{{ route('user', ['username' => $image->username]) }}">{{ $image->username }}</a>
                    </header>
                    <div>
                        <a href="{{ route('user', ['username' => $image->username]) }}">{{ $image->username }}</a>
                        <span>{{ $image->text }}</span>
                    </div>
                    <div>
                        <small>
                            {{ \Carbon\Carbon::createFromTimestamp($image->created_time)->diffForHumans() }}
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection