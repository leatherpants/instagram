<?php

namespace App\Instagram\Images\Repositories;

use App\Instagram\Images\Models\Images;

class ImagesRepository
{
    const limit = 30;

    const type = 'standard_resolution';

    /**
     * @var Images
     */
    private $images;

    /**
     * ImagesRepository constructor.
     * @param Images $images
     */
    public function __construct(Images $images)
    {
        $this->images = $images;
    }

    /**
     * Get all images.
     * @return mixed
     */
    public function getAllImages()
    {
        return $this->images
            ->get();
    }

    /**
     * Get all images by user id.
     * @param int $userid
     * @return mixed
     */
    public function getImagesByUserID(int $userid)
    {
        return $this->images
            ->where('userid', $userid)
            ->where('type', self::type)
            ->orderBy('id', 'DESC')
            ->get();
    }

    /**
     * Get latest images.
     * @return mixed
     */
    public function getLatestImages()
    {
        return $this->images
            ->join('users', 'images.userid', '=', 'users.id')
            ->join('captions', 'images.id', '=', 'captions.id')
            ->where('images.type', self::type)
            ->orderBy('images.id', 'DESC')
            ->limit(self::limit)
            ->get();
    }

    /**
     * Get image by id.
     * @param int $id
     * @return mixed
     */
    public function getImageByID(int $id)
    {
        return $this->images
            ->join('users', 'images.userid', '=', 'users.id')
            ->join('captions', 'images.id', '=', 'captions.id')
            ->where('images.type', self::type)
            ->where('images.id', $id)
            ->first();
    }
}
