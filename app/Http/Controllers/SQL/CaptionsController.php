<?php

namespace App\Http\Controllers\SQL;

use App\Http\Controllers\Controller;
use App\Instagram\Captions\Repositories\CaptionsRepository;

class CaptionsController extends Controller
{
    /**
     * @var CaptionsRepository
     */
    private $captionsRepository;

    /**
     * CaptionsController constructor.
     * @param CaptionsRepository $captionsRepository
     */
    public function __construct(CaptionsRepository $captionsRepository)
    {
        $this->captionsRepository = $captionsRepository;
    }

    /**
     * Get all captions from images.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $captions = $this->captionsRepository->getAll();

        return view('sql.captions')
            ->with('captions', $captions);
    }
}
