<?php

namespace App\Http\Controllers\SQL;

use App\Http\Controllers\Controller;
use App\Instagram\Users\Repositories\UsersRepository;

class UsersController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * UsersController constructor.
     * @param UsersRepository $usersRepository
     */
    public function __construct(UsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    /**
     * Get all users for seeders.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = $this->usersRepository->getAllByID();

        return view('sql.users')
            ->with('users', $users);
    }
}
