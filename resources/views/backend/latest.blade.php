@extends('layouts.backend')
@section('title', 'Backend • Instagram')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui cards">
                        @foreach($latests as $latest)
                            <div class="card">
                                <a href="{{ route('image', ['id' => $latest->id]) }}" target="_blank" class="image">
                                    <img src="{{ $latest->url }}" alt="">
                                    <div class="ui top left attached label">
                                        <i class="time icon"></i> {{ \Carbon\Carbon::createFromTimestamp($latest->created_time)->diffForHumans() }}
                                    </div>
                                    <div class="ui bottom right attached label">
                                        <i class="user icon"></i> {{ $latest->username }}
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection