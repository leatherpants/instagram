@extends('layouts.frontend')
@section('title', $user->full_name.' (@'.$user->username.') • Instagram')
@section('content')
    <div class="ui container">
        <div class="ui stackable grid">
            <div class="two column row">
                <div class="column">
                    <img src="{{ $user->profile_picture }}" alt="{{ $user->username }}" class="ui small centered circular image">
                </div>
                <div class="column">
                    <div style="display: flex;position: relative;flex-direction: row;">
                        <h1 class="ui header">
                            {{ $user->username }}
                        </h1>
                        @if ($user->is_verified)
                            <i class="blue check circle icon"></i>
                        @endif
                    </div>
                    <div style="display: block;">
                        <h1 class="ui header">{{ $user->full_name }}</h1>
                        @if ($user->bio)
                        <span>{{ $user->bio }}</span>
                        @endif
                        @if ($user->website)
                        <a href="{{ $user->website }}" style="font-weight: 600;display: block;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;" target="_blank">{{ $user->website }}</a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @if (count($images) > 0)
                    <div class="ui three stackable cards">
                        @foreach($images as $image)
                            <div class="card">
                                <a href="{{ route('image', ['id' => $image->id]) }}" class="image">
                                    <img src="{{ $image->url }}" alt="">
                                </a>
                            </div>
                        @endforeach
                    </div>
                    @else
                    <div class="ui placeholder segment">
                        <div class="ui icon header">
                            <i class="camera retro icon"></i>
                            No Posts Yet
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection