<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Backend', 'prefix' => 'backend'], function () {
    Route::get('', ['uses' => 'HomeController@index', 'as' => 'backend']);
    Route::get('latest', ['uses' => 'HomeController@latest', 'as' => 'backend.latest']);

    Route::group(['prefix' => 'users'], function () {
        Route::get('', ['uses' => 'UsersController@index', 'as' => 'backend.users']);
    });
});

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('', ['uses' => 'HomeController@index', 'as' => 'index']);

    Route::get('{username}', ['uses' => 'UsersController@profile', 'as' => 'user']);

    Route::get('image/{id}', ['uses' => 'ImagesController@image', 'as' => 'image']);
});

Route::group(['namespace' => 'SQL', 'prefix' => 'sql'], function () {
    Route::get('captions', ['uses' => 'CaptionsController@index', 'as' => 'sql.captions']);
    Route::get('images', ['uses' => 'ImagesController@index', 'as' => 'sql.images']);
    Route::get('users', ['uses' => 'UsersController@index', 'as' => 'sql.users']);
});
