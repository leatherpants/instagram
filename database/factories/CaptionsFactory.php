<?php

use Faker\Generator as Faker;

$factory->define(\App\Instagram\Captions\Models\Captions::class, function (Faker $faker) {
    return [
        'id' => function() {
            return factory(\App\Instagram\Images\Models\Images::class)->create()->id;
        },
        'created_time' => $faker->unixTime,
        'text' => $faker->text,
        'from' => function() {
            return factory(\App\Instagram\Users\Models\Users::class)->create()->id;
        },
    ];
});
