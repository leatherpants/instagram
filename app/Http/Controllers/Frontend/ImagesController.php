<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Instagram\Images\Repositories\ImagesRepository;

class ImagesController extends Controller
{
    /**
     * @var ImagesRepository
     */
    private $imagesRepository;

    /**
     * ImagesController constructor.
     * @param ImagesRepository $imagesRepository
     */
    public function __construct(ImagesRepository $imagesRepository)
    {
        $this->imagesRepository = $imagesRepository;
    }

    /**
     * Shows the image of a user by the id.
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function image(int $id)
    {
        $image = $this->imagesRepository->getImageByID($id);

        return view('users.image')
            ->with('image', $image);
    }
}
