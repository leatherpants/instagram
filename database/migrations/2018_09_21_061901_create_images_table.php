<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->nullable()->index();
            $table->unsignedBigInteger('userid')->nullable()->index();
            $table->string('type', 20);
            $table->string('url');
            $table->unsignedSmallInteger('width');
            $table->unsignedSmallInteger('height');
            $table->unique(['id', 'userid', 'type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
