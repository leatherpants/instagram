<?php

namespace Tests\Feature;

use Tests\TestCase;

class ImagesTest extends TestCase
{
    /** @test */
    public function test_shows_image()
    {
        $response = $this->get(route('image', ['id' => $this->image->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_image_fails()
    {
        $response = $this->get(route('image', ['id' => 0]));
        $response->assertNotFound();
    }
}
