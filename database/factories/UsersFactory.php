<?php

use Faker\Generator as Faker;

$factory->define(\App\Instagram\Users\Models\Users::class, function (Faker $faker) {
    return [
        'id' => $faker->randomNumber,
        'username' => $faker->userName,
        'full_name' => $faker->name,
        'profile_picture' => $faker->imageUrl,
        'bio' => $faker->text,
        'website' => $faker->url,
        'is_active' => $faker->boolean,
        'is_private' => $faker->boolean,
        'is_verified' => $faker->boolean,
    ];
});
