<?php

namespace Tests\Feature\SQL;

use Tests\TestCase;

class ImagesTest extends TestCase
{
    /** @test */
    public function test_get_all_images()
    {
        $response = $this->get(route('sql.images'));
        $response->assertSuccessful();
    }
}
