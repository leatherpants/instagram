<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class UsersTest extends TestCase
{
    /** @test */
    public function test_get_all_users()
    {
        $response = $this->get(route('backend.users'));
        $response->assertSuccessful();
    }
}
