<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Instagram\Images\Repositories\ImagesRepository;

class HomeController extends Controller
{
    /**
     * @var ImagesRepository
     */
    private $imagesRepository;

    /**
     * HomeController constructor.
     * @param ImagesRepository $imagesRepository
     */
    public function __construct(ImagesRepository $imagesRepository)
    {
        $this->imagesRepository = $imagesRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function latest()
    {
        $latests = $this->imagesRepository->getLatestImages();

        return view('backend.latest')
            ->with('latests', $latests);
    }
}
