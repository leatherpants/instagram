<?php

namespace App\Instagram\Captions\Repositories;

use App\Instagram\Captions\Models\Captions;

class CaptionsRepository
{
    /**
     * @var Captions
     */
    private $captions;

    /**
     * CaptionsRepository constructor.
     * @param Captions $captions
     */
    public function __construct(Captions $captions)
    {
        $this->captions = $captions;
    }

    /**
     * Get all captions.
     * @return mixed
     */
    public function getAll()
    {
        return $this->captions
            ->get();
    }
}
