<?php

namespace App\Instagram\Users\Repositories;

use App\Instagram\Users\Models\Users;

class UsersRepository
{
    /**
     * @var Users
     */
    private $users;

    /**
     * UsersRepository constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * Get all users.
     * @return mixed
     */
    public function getAll()
    {
        return $this->users
            ->orderBy('username')
            ->get();
    }

    /**
     * Get all users by id.
     * @return mixed
     */
    public function getAllByID()
    {
        return $this->users
            ->orderBy('id')
            ->get();
    }

    /**
     * Get data for user's profile by his username.
     * @param string $username
     * @return mixed
     */
    public function getUserByUserName(string $username)
    {
        return $this->users
            ->where('username', $username)
            ->first();
    }
}
