<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="{{ asset('assets/semantic.min.css') }}" rel="stylesheet">
    <style type="text/css">
        body {
            background-color: #FFFFFF;
        }
        .main.container {
            margin-top: 7em;
        }
    </style>
</head>
<body>
    <div class="ui fixed huge inverted menu">
        <div class="ui container">
            <a href="{{ route('backend') }}" class="icon item">
                <i class="instagram icon"></i>
            </a>
            <div class="right menu">
                <a href="{{ route('backend.users') }}" class="icon item">
                    <i class="users icon"></i>
                </a>
            </div>
        </div>
    </div>
@yield('content')
<script src="{{ asset('assets/jquery.min.js') }}"></script>
<script src="{{ asset('assets/semantic.min.js') }}"></script>
</body>
</html>