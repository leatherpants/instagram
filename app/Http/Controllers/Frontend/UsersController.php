<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Instagram\Users\Repositories\UsersRepository;
use App\Instagram\Images\Repositories\ImagesRepository;

class UsersController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * @var ImagesRepository
     */
    private $imagesRepository;

    /**
     * UsersController constructor.
     * @param UsersRepository $usersRepository
     * @param ImagesRepository $imagesRepository
     */
    public function __construct(UsersRepository $usersRepository,
        ImagesRepository $imagesRepository)
    {
        $this->usersRepository = $usersRepository;
        $this->imagesRepository = $imagesRepository;
    }

    /**
     * Show the user profile by the username.
     * @param string $username
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile(string $username)
    {
        $user = $this->usersRepository->getUserByUserName($username);

        if (!$user) {
            abort(404);
        }

        $images = $this->imagesRepository->getImagesByUserID($user->id);

        return view('users.profile')
            ->with('user', $user)
            ->with('images', $images);
    }
}
