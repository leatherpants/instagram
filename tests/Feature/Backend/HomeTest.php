<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class HomeTest extends TestCase
{
    /** @test */
    public function test_get_latest_images()
    {
        $response = $this->get(route('backend.latest'));
        $response->assertSuccessful();
    }
}
