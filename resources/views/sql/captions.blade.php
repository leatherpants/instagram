@extends('layouts.blank')
@section('content')
DB::table('captions')->insert([
@foreach($captions as $caption)
    [
        'id' => {{ $caption->id }},
        'created_time' => {{ $caption->created_time }},
        'text' => '{{ $caption->text }}',
        'from' => {{ $caption->from }},
    ],
@endforeach
]);
@endsection