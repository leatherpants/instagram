@extends('layouts.backend')
@section('title', 'Backend • Instagram')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui placeholder segment">
                        <div class="ui icon header">
                            <i class="users icon"></i>
                            {{ count($users) }}
                            {{ trans('common.users') }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui four stackable cards">
                        @foreach($users as $user)
                            <div class="card">
                                <div class="image">
                                    <img src="{{ $user->profile_picture }}" alt="{{ $user->username }}">
                                </div>
                                <div class="content">
                                    <a href="{{ route('user', ['username' => $user->username]) }}" target="_blank" class="header">{{ $user->username }}</a>
                                    <a href="https://www.instagram.com/{{ $user->username }}/" target="_blank"><i class="external icon"></i></a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection